<?php
$routes = array(
    ''=>[
        ''=>[\Controllers\GeneralController::class,'index']
    ],
    'inicio'=>[
        ''=>[\Controllers\GeneralController::class,'index'],
    ],
    'ajax'=>[
        ''=>[\Controllers\GeneralController::class,'ajax']
    ]
);

if($routes[MODULE][ACTION][0]!="") {
    $controller = new $routes[MODULE][ACTION][0];
    $fsign = strpos(CRUD, '?');
    $keyAction = (CRUD != '' && ($fsign === false)) ? ACTION . '/' . CRUD : ACTION;
    $function = $routes[MODULE][$keyAction][1];
    if (method_exists($controller, $function)) {
        if (PARAMETER != '') {
            $controller->$function(PARAMETER);
        } else {
            $controller->$function();
        }
    } else {
        include_once('resources/views/404.php');
    }
}else {
    if(MODULE=='login') {
        $controller = new $routes[MODULE][''][0];
        $function="login";
        $controller->$function();
    }else {
        include_once('resources/views/404.php');
    }
}