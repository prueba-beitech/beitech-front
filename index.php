<?php
session_start();
date_default_timezone_set('America/Bogota');
ini_set('display_errors', 1);
#error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);
require_once  'vendor/autoload.php';

//Cargamos configuraciones basicas
$isRootAPP = ($_SERVER['SERVER_NAME']=='localhost'||$_SERVER['SERVER_NAME']=='127.0.0.1')?true:false;
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__, '.env');
$dotenv->load();
define('URL_API', $_ENV['URL_API']);
define('ROOT', (($isRootAPP)?$_ENV['ROOT_QA']:$_ENV['ROOT_APP']));
define('SERVER_HOST', (($isRootAPP)?$_ENV['SERVER_HOST_QA']:$_ENV['SERVER_HOST']));
define('SITE',SERVER_HOST.ROOT);
define('VIEW',ROOT.'/resources');


//Define los elementos según la url
$url = explode("/", $_SERVER['REQUEST_URI']);
$module =($isRootAPP)?$url[2]:$url[1];
$action =($isRootAPP)?$url[3]:$url[2];
$crud =($isRootAPP)?$url[4]:$url[3];
$parameter =($isRootAPP)?$url[5]:$url[4];

define ('MODULE',$module);
define ('ACTION',$action);
define ('CRUD',$crud);
define ('PARAMETER',$parameter);

require_once 'routes.php';