<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.84.0">
    <title>Prueba PHP Beitech SAS </title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/checkout/">



    <!-- Bootstrap core CSS -->
    <link href="<?php echo VIEW ?>/views/css/bootstrap.min.css" rel="stylesheet">

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>


    <!-- Custom styles for this template -->
    <link href="<?php echo VIEW ?>/views/form-validation.css" rel="stylesheet">
</head>
<body class="bg-light">

<div class="container">
    <main>
        <div class="py-5 text-center">
        </div>

        <div class="row g-5">
            <div class="col-md-7 col-lg-8">
                <h4 class="mb-3">Orders</h4>
                <form class="needs-validation" novalidate>
                    <div class="row g-3">
                        <div class="col-md-5">
                            <label for="customer_id" class="form-label">Customers</label>
                            <select class="form-select" id="customer_id" name="customer_id" required>
                                <option value="">Choose one...</option>
                                <?php
                                foreach ($customer AS $cus){
                                    echo '<option value="'.$cus['customer_id'].'">'.$cus['name'].'</option>';
                                }
                                ?>
                            </select>
                            <div class="invalid-feedback">
                                Please select a valid country.
                            </div>
                        </div>

                        <div class="col-md-4">
                            <label for="date_from" class="form-label">Date from</label>
                            <input type="date" id="date_from" name="date_from" class="form-control">
                        </div>

                        <div class="col-md-3">
                            <label for="date_to" class="form-label">Date To</label>
                            <input type="date" id="date_to" name="date_to" class="form-control">
                        </div>
                    </div>


                    <hr class="my-4">

                    <button class="btn btn-primary btn-lg" type="button" id="apply_filter">Apply filter</button>
                </form>
            </div>
        </div>
        <div class="py-5 text-center">
        </div>

        <div class="row g-5">
            <div class="col-md-11">
                <table id="dataCustomer" class="table table-bordered table-striped" width="100%">
                    <thead>
                    <th>Creation Date</th>
                    <th>Order Id</th>
                    <th>Total</th>
                    <th>Address</th>
                    <th>Products</th>
                    </thead>
                    <tbody id="bodyCus">

                    </tbody>
                </table>
            </div>
            <hr>
            <br>
    </main>
</div>


<script src="<?php echo VIEW ?>/views/js/bootstrap.bundle.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap5.min.css"/>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap5.min.js"></script>
<script src="<?php echo VIEW ?>/views/js/form-validation.js"></script>
<script>
    $(document).ready(function() {
        var date_to;
        var date_from;
        var customer_id;
        $("#date_to").change(function (){
            date_to =  $(this).val();
        });
        $("#date_from").change(function (){
            date_from =  $(this).val();
        });
        $("#customer_id").change(function (){
            customer_id =  $(this).val();
        });
        $("#apply_filter").click( function (){
            $.post('<?php echo ROOT ?>/ajax', {
                'type':'sess',
                "date_to": date_to,
                "date_from": date_from,
                "customer_id": customer_id
            }, function(data) {});

            $("#dataCustomer").DataTable().ajax.reload();
            $("#dataCustomer").DataTable().draw();
        });

        $('#dataCustomer').dataTable({
            "responsive": true,
            "bProcessing": true,
            "ajax": {
                "url":  "<?php echo ROOT ?>/ajax",
                "type": "POST",
                "data":{
                    "date_to": date_to,
                    "date_from": date_from,
                    "customer_id": customer_id
                }
            },
            "pageLength": 12,
            "aoColumns": [
                { mData: 'creation_date' },
                { mData: 'order_id' },
                { mData: 'total' },
                { mData: 'delivery_address' },
                { mData: 'products_list' }
            ]
        });
    } );
</script>
</body>
</html>