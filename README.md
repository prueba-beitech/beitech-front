# FRONT BEITECH sas


- Para iniciar el proyecto es necesario ejecutar el siguiente comando en la raiz del proyecto
```composer
composer update
```
- Los datos de configuración deben setearse en un archivo llamado <code>.env</code> en la raiz del proyecto. Tomar como ejemplo el archivo <code>.env.example</code>.
- En el <code>.env</code>, poner la ip o url de la api en la variable <code>URL_API</code>.
- La raíz del proyecto debe ir definida en el archivo (ejemplo /beitech-front/ ) <code>.htaccess</code> en la raiz del proyecto. Tomar como ejemplo el archivo <code>.htaccess.example</code>
