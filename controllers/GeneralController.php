<?php
namespace  Controllers;
Use Services;
use Dotenv\Dotenv;
class GeneralController
{

    function index(){
        $objApi = new Services\ConsumerApi();

        $customer = $objApi->perform_http_request('GET',URL_API.'/api/customer');
        $customer = json_decode($customer,true);
        $customer = $customer['data']['customer'];
        include_once('resources/views/master.php');
    }

    function ajax()
    {
        if ($_POST['type'] == 'sess') {

            $_SESSION['customer_id'] = $_POST['customer_id'];
            $_SESSION['date_to'] = $_POST['date_to'];
            $_SESSION['date_from'] = $_POST['date_from'];

        } else {
            $objApi = new Services\ConsumerApi();
            $data = array(
                'customer_id' => ($_SESSION['customer_id'] == '') ? null : $_SESSION['customer_id'],
                'date_to' => ($_SESSION['date_to'] == '') ? null : $_SESSION['date_to'],
                'date_from' => ($_SESSION['date_from'] == '') ? null : $_SESSION['date_from']
            );
            $customer = $objApi->perform_http_request('GET', URL_API . '/api/orders', $data);
            $customer = json_decode($customer, true);
            $customer = $customer['data']['orders'];

            $data = array();
            foreach ($customer as $row) {
                $arrayB = $row;
                $dataprd = "";
                if (count($row['order_detail']) > 0) {

                    foreach ($row['order_detail'] as $d) {
                        $dataprd .= $d['quantity'] . " x " . $d['product_description'] . '<br>';
                    }
                } else {
                    $dataprd .= '';
                }
                $arrayB['products_list'] = $dataprd;
                $data[] = $arrayB;
            }

            $results = ["sEcho" => 1,
                "iTotalRecords" => count($data),
                "iTotalDisplayRecords" => count($data),
                "aaData" => $data];
            echo json_encode($results);

        }

    }
}